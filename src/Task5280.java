import HW.*;
public class Task5280 {
    public static void main(String[] args) throws Exception {
        //task 1 - get rounded
        Task1 case1 = new Task1(2.100212, 2);
        Task1 case2 = new Task1(2.100212, 3);
        Task1 case3 = new Task1(2100, 2);
        System.out.println("Task 1 - The value of number 2.100212 rounded by 2 digits: " + case1.getRounded());
        System.out.println("Task 1 - The value of number 2.100212 rounded by 3 digits: " + case2.getRounded());
        System.out.println("Task 1 - The value of number 2100 rounded by 2 digits: " + case3.getRounded());
        //task 2 - get random
        Task2 random = new Task2(2, 10);
        Task2 random2 = new Task2(10, 20);
        System.out.println("Task 2 - random number between 2 and 10: " + random.getRandom());
        System.out.println("Task 2 - random number between 10 and 20: " + random2.getRandom());
        //Task 3
        Task3 pytago1 = new Task3(2,4);
        Task3 pytago2 = new Task3(3,4);
        System.out.println("Task 3 - pytago 1 is: " + pytago1.getPytago());
        System.out.println("Task 3 - pytago 2 is: " + pytago2.getPytago());
        //task 4
        Task4 number1 = new Task4(64);
        Task4 number2 = new Task4(92);
        System.out.println("Task 4 - kiem tra so chinh phuong 64: " + number1.checkSquare());
        System.out.println("Task 4 - kiem tra so chinh phuong 92: " + number2.checkSquare());
        //task 5
        Task5 number5a = new Task5(32);
        Task5 number5b = new Task5(137);
        System.out.println("Task 5 - so tiep theo chia het cho 5 cua so 32: " + number5a.getNextNumber());
        System.out.println("Task 5 - so tiep theo chia het cho 5 cua so 137: " + number5b.getNextNumber());
        //task 6
        Task6 input6a = new Task6(12);
        Task6 input6b = new Task6("abcd");
        Task6 input6c = new Task6("12");
        System.out.println("Task 6 - input 1 is integer: " + input6a.isObjectInteger());
        System.out.println("Task 6 - input 2 is integer: " + input6b.isObjectInteger());
        System.out.println("Task 6 - input 3 is integer: " + input6c.isObjectInteger());
        //task 7
        Task7 number7a = new Task7(16);
        Task7 number7b = new Task7(18);
        Task7 number7c = new Task7(256);
        System.out.println("Task 7 - check if 16 is power of 2: " + number7a.isPowerOfTwo());
        System.out.println("Task 7 - check if 18 is power of 2: " + number7b.isPowerOfTwo());
        System.out.println("Task 7 - check if 256 is power of 2: " + number7c.isPowerOfTwo());
        //task 8
        Task8 number8a = new Task8(-15);
        Task8 number8b = new Task8(1);
        Task8 number8c = new Task8(1.2);
        System.out.println("Task 8 - kiem tra so tu nhien 1: " + number8a.isNeutralNumber());
        System.out.println("Task 8 - kiem tra so tu nhien 2: " + number8b.isNeutralNumber());
        System.out.println("Task 8 - kiem tra so tu nhien 3: " + number8c.isNeutralNumber());
        //task 9
        Task9 number9a = new Task9(1000);
        Task9 number9b = new Task9(10000.21f);
        System.out.println("Task 9 - format number 1: " + number9a);
        System.out.println("Task 9 - format number 2: " + number9b);
        //task 10
        int number10a = 51;
        int number10b = 4;
        System.out.println("Task 10 - chuyen so" + number10a + "sang he nhi phan: " + Integer.toBinaryString(number10a));
        System.out.println("Task 10 - chuyen so" + number10b + "sang he nhi phan: " + Integer.toBinaryString(number10b));
    }
}
