package HW;
import java.text.DecimalFormat;
public class Task9 {
    private float number;

    public Task9(float number) {
        this.number = number;
    }
    @Override
    public String toString() {
        //add formatter
        DecimalFormat myFormatter = new DecimalFormat("###,###.###");
        String output = myFormatter.format(number);
        return "Task9 " + number + ": " + " " + output;
    }

    
}
