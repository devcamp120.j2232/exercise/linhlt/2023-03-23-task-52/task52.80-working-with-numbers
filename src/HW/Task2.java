package HW;
import java.util.Random;
import java.util.ArrayList;
public class Task2 {
    private int begin;
    private int end;
    
    public Task2(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }
    
    public int getRandom() {
        ArrayList<Integer> arrayResult = new ArrayList<Integer>();
        for (int element = begin; element <= end; element++){
            arrayResult.add(element);
        }
        System.out.println(arrayResult);
        int rnd = new Random().nextInt(arrayResult.size());
        return arrayResult.get(rnd);
    }
}
