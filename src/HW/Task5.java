package HW;

public class Task5 {
    private int number;

    public Task5(int number) {
        this.number = number;
    }
    
    public int getNextNumber(){
        int soDu = number % 5;
        int nextNumber = number; 
        if (soDu == 0){
            nextNumber = number;
        }
        else if (soDu == 1){
            nextNumber = number + 4;
        }
        else if (soDu == 2){
            nextNumber = number + 3;
        }
        else if (soDu == 3){
            nextNumber = number + 2;
        }
        else if (soDu == 4){
            nextNumber = number + 1;
        }
        return nextNumber;
    }
}
