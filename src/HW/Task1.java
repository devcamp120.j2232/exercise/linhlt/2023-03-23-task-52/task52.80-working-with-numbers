package HW;
// round() method
import java.math.*;
public class Task1 {
    private double number;
    private int m; //level of precision
    public Task1(double number, int m) {
        this.number = number;
        this.m = m;
    }
    public BigDecimal getRounded(){
        BigDecimal b1 = new BigDecimal(number);
        MathContext precision = new MathContext(m+1);
        return b1.round(precision);
    }
}
