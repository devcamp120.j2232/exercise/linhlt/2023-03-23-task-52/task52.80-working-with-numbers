package HW;

public class Task6 {
    private Object input;

    public Task6(Object input) {
        this.input = input;
    }
    
    public boolean isObjectInteger(){
        return input instanceof Integer;
    }
}
