package HW;

public class Task7 {
    private int number;

    public Task7(int number) {
        this.number = number;
    }
    public boolean isPowerOfTwo(){
        while (number % 2 == 0){
            number = number /2;
        }
        if (number == 1){
            return true;
        } 
        else return false;
    }
}
